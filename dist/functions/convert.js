"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var csv = require("csv-parser");
var fs = __importStar(require("fs"));
var os = __importStar(require("os"));
var logger;
logger = fs.createWriteStream('helper/data.txt', { flags: 'a' });
exports.convertCsv = function () {
    fs.createReadStream('helper/new.csv')
        .pipe(csv())
        .on('data', function (row) {
        logger.write(os.EOL);
        logger.write(JSON.stringify(row) + os.EOL);
    })
        .on('end', function () {
        console.log('CSV file successfully processed');
    });
};
