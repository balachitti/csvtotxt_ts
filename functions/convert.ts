import csv = require('csv-parser');
import * as fs from 'fs';
import * as os from 'os';
 var logger : any;
 
logger = fs.createWriteStream('helper/data.txt', { flags: 'a' })

export const convertCsv = () => {

    fs.createReadStream('helper/new.csv')
  .pipe(csv())
  .on('data', (row: any) => {
     
    logger.write( os.EOL)
    logger.write(JSON.stringify(row) + os.EOL)
})
.on('end', () => {
    console.log('CSV file successfully processed')
})
}

