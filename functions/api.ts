import * as request from "request";

export function initialize() {
    // Setting URL and headers for request
    var options = {
        url: 'https://jsonplaceholder.typicode.com/todos/',
        headers: {
            'User-Agent': 'request'
        }
    };
    // Return new promise 
    return new Promise(function(resolve, reject) {
     // Do async job
        request.get(options, function(err: any, resp: any, body: string) {
            if (err) {
                reject(err);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })
}
